package be.vdab.shoppingCenter.person;

import be.vdab.shoppingCenter.cart.ICart;
import be.vdab.shoppingCenter.gender.Gender;
import be.vdab.shoppingCenter.store.IStore;

/**
 * @author Behlül Savaskurt
 * created on 21/07/2021
 */

public class Person implements IPerson {

    private String name;
    private Gender gender;
    private int age;
    private ICart cart;

    public Person() {
    }

    public Person(String name, Gender gender, int age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    public Person(String name, Gender gender, int age, ICart cart) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.cart = cart;
    }

    public String getName() {
        return name;
    }

    public Gender getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public ICart getCart() {
        return cart;
    }

    public void walkToStore(IStore iStore) {
        System.out.println("Walking towards " + iStore.getName() + ".");

        try {
            Thread.sleep(500);
            System.out.println(". ");
            Thread.sleep(500);
            System.out.println(". ");
            Thread.sleep(500);
            System.out.println(". ");

        } catch (InterruptedException ie) {
            System.err.println("Dit zou je niet mogen zien.");
        }
    }

    @Override
    public String toString() {
        return "Person:\n" +
                "\nName : " + name + "\n\n" +
                "Gender : " + gender + "\n\n" +
                "Age : " + age + "\n";
    }
}
