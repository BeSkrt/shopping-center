package be.vdab.shoppingCenter.person;

import be.vdab.shoppingCenter.cart.ICart;
import be.vdab.shoppingCenter.cart.SmallCart;
import be.vdab.shoppingCenter.gender.Gender;
import be.vdab.shoppingCenter.store.IStore;
import be.vdab.shoppingCenter.utility.KeyboardHelper;

import java.io.Serializable;

/**
 * @author Behlül Savaskurt
 * created on 21/07/2021
 */

public interface IPerson extends Serializable {

    void walkToStore(IStore iStore);

    /**
     * Static factory method for Person.
     *
     * @param name   = name of the person.
     * @param gender = gender of the person.
     * @param age    = age of the person.
     * @param cart   = shopping cart of the person.
     * @return = returns new Person object.
     */
    static IPerson createPerson(String name, Gender gender, int age, ICart cart) {
        return new Person(name, gender, age, cart);
    }

    String getName();
    Gender getGender();
    int getAge();
    ICart getCart();
}
