package be.vdab.shoppingCenter.main;

import be.vdab.shoppingCenter.person.IPerson;
import be.vdab.shoppingCenter.store.*;
import be.vdab.shoppingCenter.utility.KeyboardHelper;
import be.vdab.shoppingCenter.utility.RegistryHelper;
import be.vdab.shoppingCenter.utility.TimeHelper;

/**
 * @author Behlül Savaskurt
 * created on 21/07/2021
 */

public class StoreApp {
    public static IPerson person;
    public static ShoppingCenter mall;

    public static void main(String[] args) {

        person = RegistryHelper.registerOrLogIn();

//        person = new Person(KeyboardHelper.askForName(), KeyboardHelper.askForGender(), KeyboardHelper.askForAge(), new SmallCart());

        mall = new ShoppingCenter();
        startSimulation(person, mall);

        KeyboardHelper.close();
    }

    public static void startSimulation(IPerson person, ShoppingCenter mall) {
        System.out.println(person.toString());
        TimeHelper.getLocalDateTime();
        System.out.println("Let's go to " + mall.getName() + "!");
        person.walkToStore(mall);

        boolean leftShoppingCenter;
        do {
            mall.enterStore(person);
            leftShoppingCenter = mall.getLeftShoppingCenter();
        } while (!leftShoppingCenter);
    }

}
