package be.vdab.shoppingCenter.cart;

import java.io.Serializable;

/**
 * @author Behlül Savaskurt
 * created on 21/07/2021
 */

public interface ICart extends Serializable {

    boolean isFull();
    int getItemCount();
}
