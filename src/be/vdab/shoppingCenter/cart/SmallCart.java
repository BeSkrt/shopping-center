package be.vdab.shoppingCenter.cart;

/**
 * @author Behlül Savaskurt
 * created on 21/07/2021
 */

public class SmallCart implements ICart {

    private static final int MAX_ITEMS = 10;
    private int itemCount = 0;
    private String itemList;
    private boolean full;

    public SmallCart() {
    }

    public static int getMaxItems() {
        return MAX_ITEMS;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public String getItemList() {
        return itemList;
    }

    public void setItemList(String itemList) {
        this.itemList = itemList;
    }

    public boolean isFull() {
        return full;
    }

    public void setFull(boolean full) {
        this.full = full;
    }
}
