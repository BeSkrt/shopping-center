package be.vdab.shoppingCenter.store;

import java.util.Arrays;

/**
 * @author Behlül Savaskurt
 * created on 21/07/2021
 */

public class Bar extends ItemStore {

    private static final String NAME = "Donovan's";
    private static final String DESCRIPTION = "A place to sit down and enjoy a drink. With all that shopping comes a lot of stress, time to unwind!";

    public Bar() {
        super(new String[]{"Water", "Coffee", "Tea", "Coca-Cola", "Beer", "Whiskey", "Wine"});
    }

    public Bar(String[] items) {
        super(items);
    }

    public static String getNAME() {
        return NAME;
    }

    public static String getDESCRIPTION() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String toString() {
        return "Bar{" +
                "items=" + Arrays.toString(getItems()) +
                '}';
    }
}
