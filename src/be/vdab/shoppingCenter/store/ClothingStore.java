package be.vdab.shoppingCenter.store;

import be.vdab.shoppingCenter.person.IPerson;
import be.vdab.shoppingCenter.person.Person;

import java.util.Arrays;

/**
 * @author Behlül Savaskurt
 * created on 21/07/2021
 */

public class ClothingStore extends ItemStore {

    private static final String NAME = "The Adidas Store";
    private static final String DESCRIPTION = "A place to buy fashionable clothing. The clothing here is right up your alley.";

    public ClothingStore() {
        super(new String[]{"Track vest", "Sneakers", "Hoodie", "Training suit"});
    }

    public ClothingStore(String[] items) {
        super(items);
    }

    public static String getNAME() {
        return NAME;
    }

    public static String getDESCRIPTION() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String toString() {
        return "ClothingStore{" +
                "items=" + Arrays.toString(getItems()) +
                '}';
    }
}
