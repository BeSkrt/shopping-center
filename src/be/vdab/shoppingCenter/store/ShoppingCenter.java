package be.vdab.shoppingCenter.store;

import be.vdab.shoppingCenter.person.IPerson;
import be.vdab.shoppingCenter.utility.KeyboardHelper;

/**
 * @author Behlül Savaskurt
 * created on 21/07/2021
 */

public class ShoppingCenter implements IStore {

    private static final String NAME = "The Complex";
    private static final String DESCRIPTION = "a shopping center.";
    public static IStore[] stores;
    private boolean leftShoppingCenter = false;

    public ShoppingCenter() {
        stores = new IStore[]{new ComputerStore(), new ClothingStore(), new Bar()};
    }

    public boolean getLeftShoppingCenter() {
        return leftShoppingCenter;
    }

    private void setLeftShoppingCenter(boolean leftShoppingCenter) {
        this.leftShoppingCenter = leftShoppingCenter;
    }

    public IStore[] getStores() {
        return stores;
    }

    private void handleChoice(IPerson person, int choice) {
        // TODO: exceptions!!!

        switch(choice) {
            case 0:
                System.out.println("Leaving " + ShoppingCenter.NAME + ". See you next time!");
                setLeftShoppingCenter(true);
                break;
            case 1:
                person.walkToStore(stores[0]);
                stores[0].enterStore(person);
                break;
            case 2:
                person.walkToStore(stores[1]);
                stores[1].enterStore(person);
                break;
            case 3:
                person.walkToStore(stores[2]);
                stores[2].enterStore(person);
                break;
            default:
                break;
        }
    }

    private static void showMenu() {
        System.out.println("You are at the mall. You see three stores you'd want to visit. Where do you want to go?");
        System.out.printf("1. %s : %s%n", ComputerStore.getNAME(), ComputerStore.getDESCRIPTION());
        System.out.printf("2. %s : %s%n", ClothingStore.getNAME(), ClothingStore.getDESCRIPTION());
        System.out.printf("3. %s : %s%n", Bar.getNAME(), Bar.getDESCRIPTION());
        System.out.println("0. Leave");
    }

    private static int getChoice() {
        return KeyboardHelper.askForChoice();
    }

    @Override
    public void enterStore(IPerson person) {
        System.out.println(person.getName() + " enters " + ShoppingCenter.NAME + ", " + ShoppingCenter.DESCRIPTION);
        showMenu();
        handleChoice(person, getChoice());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
