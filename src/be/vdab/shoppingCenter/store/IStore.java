package be.vdab.shoppingCenter.store;

import be.vdab.shoppingCenter.person.IPerson;

/**
 * @author Behlül Savaskurt
 * created on 21/07/2021
 */

public interface IStore {

    void enterStore(IPerson person);

    String getName();
    String getDescription();

}
