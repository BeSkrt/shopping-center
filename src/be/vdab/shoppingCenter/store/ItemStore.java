package be.vdab.shoppingCenter.store;

import be.vdab.shoppingCenter.person.IPerson;
import be.vdab.shoppingCenter.utility.KeyboardHelper;

/**
 * @author Behlül Savaskurt
 * created on 21/07/2021
 */

public abstract class ItemStore implements IStore {

    private String name;
    private String description;

    private String[] items;

    public ItemStore(String[] items) {
        this.items = items;
    }

    public ItemStore() {
    }

    public String[] getItems() {
        return items;
    }

    @Override
    public abstract String getName();

    @Override
    public abstract String getDescription();

    private void handleChoice(IPerson person, int choice) {
        switch(choice) {
            case 0:
                System.out.println("Leaving " + getName() + ".");
                break;
            case 1:
                person.walkToStore(ShoppingCenter.stores[0]);
                ShoppingCenter.stores[0].enterStore(person);
                break;
            case 2:
                person.walkToStore(ShoppingCenter.stores[1]);
                ShoppingCenter.stores[1].enterStore(person);
                break;
            case 3:
                person.walkToStore(ShoppingCenter.stores[2]);
                ShoppingCenter.stores[2].enterStore(person);
                break;
            default:
                break;
        }
    }

    private void showMenu() {
        System.out.println("Items for sale: " + getItems().length);
        System.out.println("0. Leave");
        for (int i = 0; i< getItems().length; i++) {
            System.out.printf("%d. %s%n", i+1, getItems()[i]);
        }
    }

    public void enterStore(IPerson person) {
        System.out.println(person.getName() + " enters " + getName() + ", " + getDescription());
        showMenu();
        handleChoice(person, getChoice());
    }

    private int getChoice() {
        return KeyboardHelper.askForChoice();
    }

}
