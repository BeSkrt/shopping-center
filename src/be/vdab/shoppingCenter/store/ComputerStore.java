package be.vdab.shoppingCenter.store;

import be.vdab.shoppingCenter.person.IPerson;
import java.util.Arrays;

/**
 * @author Behlül Savaskurt
 * created on 21/07/2021
 */

public class ComputerStore extends ItemStore {

    private static final String NAME = "Computers R Us";
    private static final String DESCRIPTION = "A place to buy computer equipment or upgrade components. You've wanted a new monitor for a while now.";

    public ComputerStore() {
        super(new String[]{"Gaming Laptop", "Gaming Computer", "Gaming Monitor", "Gaming headset"});
    }

    public static String getNAME() {
        return NAME;
    }

    public static String getDESCRIPTION() {
        return DESCRIPTION;
    }



    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }


    @Override
    public String toString() {
        return "ComputerStore{" +
                "items=" + Arrays.toString(getItems()) +
                '}';
    }
}
