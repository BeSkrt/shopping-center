package be.vdab.shoppingCenter.utility;

import java.time.LocalDateTime;

/**
 * @author Behlül Savaskurt
 * created on 23/07/2021
 */
public class TimeHelper {

    public static void getLocalDateTime() {
        LocalDateTime now = LocalDateTime.now();
        System.out.format("Today, %1$td/%1$tm/%1$tY %1$tH:%1$tM:%1$tS%n%n", now);
    }
}
