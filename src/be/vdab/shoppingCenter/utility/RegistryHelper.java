package be.vdab.shoppingCenter.utility;

import be.vdab.shoppingCenter.cart.SmallCart;
import be.vdab.shoppingCenter.person.IPerson;

import java.io.*;
import java.nio.file.Path;
import java.util.InputMismatchException;

/**
 * @author Behlül Savaskurt
 * created on 6/08/2021
 */
public class RegistryHelper {

    public static IPerson registerOrLogIn() {
        IPerson person = null;

        while (person == null) {
            System.out.println("Welcome to the shopping center app. Would you like to register or log in?");
            System.out.println("1. Register");
            System.out.println("2. Log in");
            int choice = KeyboardHelper.askForChoice();
            switch (choice) {
                case 1:
                    person = RegistryHelper.writeToRegistry();
                    break;
                case 2:
                    person = RegistryHelper.readFromRegistry(KeyboardHelper.askForName());
                    break;
                default:
                    System.err.println("Invalid input. Please enter \"1\" or \"2\".");
                    break;
            }
        }
        return person;
    }

    public static IPerson writeToRegistry() {
        IPerson person = IPerson.createPerson(KeyboardHelper.askForName(), KeyboardHelper.askForGender(), KeyboardHelper.askForAge(), new SmallCart());

        try (FileOutputStream fileOutputStream = new FileOutputStream(".\\src\\be\\vdab\\shoppingCenter\\registry\\" + person.getName() + ".txt");
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)
        ) {
            objectOutputStream.writeObject(person);
        } catch (IOException io) {
            System.err.println("Could not register said person.\n");
        }
        return person;
    }

    public static IPerson readFromRegistry(String name) {
        Path path = Path.of(String.format(".\\src\\be\\vdab\\shoppingCenter\\registry\\%s.txt", name));

        try (FileInputStream fileInputStream = new FileInputStream(path.toString());
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ) {
            return (IPerson) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException io) {
            System.err.println("Could not find said person in the registry.\n");
        }
        return null;
    }
}
