package be.vdab.shoppingCenter.utility;

import be.vdab.shoppingCenter.gender.Gender;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author Behlül Savaskurt
 * created on 21/07/2021
 */

public class KeyboardHelper {

    private static final Scanner keyboard = new Scanner(System.in);

    public static int askForChoice() {
        int choice = 0;
        boolean validNumber = false;
        // TODO : exceptions!!!
        while (!validNumber) {
            try {
                System.out.println("Please make a choice: ");
                choice = keyboard.nextInt();
                keyboard.nextLine();
                validNumber = true;
            } catch (InputMismatchException e) {
                System.err.println("Invalid input. Please enter a number corresponding with the menu.");
            }
        }
        return choice;
    }

    public static int askForAge() {
        // TODO : exceptions!!!
        int age;
        System.out.println("Please enter your age: ");
        age = keyboard.nextInt();
        return age;
    }

    public static String askForName() {
        // TODO : exceptions!!!
        String name;
        System.out.println("Please enter your name: ");
        name = keyboard.nextLine();
        return name;
    }

    public static Gender askForGender() {
        Gender gender = null;
        while (gender == null) {
            try {
                System.out.println("Please enter your gender (M/F): ");

                switch (keyboard.next()) {
                    case "M":
                    case "m":
                    case "male":
                    case "Male":
                        gender = Gender.MALE;
                        break;
                    case "F":
                    case "f":
                    case "female":
                    case "Female":
                        gender = Gender.FEMALE;
                        break;
                    default:
                        System.err.println("Invalid input. Please enter \"M\", \"m\", \"Male\" or \"male\" for male and \"F\", \"f\", \"Female\" or \"female\" for female.");
                        break;
                }
            } catch (InputMismatchException e) {
                System.err.println("Invalid input. Please enter \"M\", \"m\", \"Male\" or \"male\" for male and \"F\", \"f\", \"Female\" or \"female\" for female.");
            }
        }
        return gender;
    }

    public static void close() {
        keyboard.close();
    }

}
