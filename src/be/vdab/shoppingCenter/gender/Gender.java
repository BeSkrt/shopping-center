package be.vdab.shoppingCenter.gender;

/**
 * @author Behlül Savaskurt
 * created on 21/07/2021
 */

public enum Gender {

    MALE,
    FEMALE
}
